const mongoose = require("mongoose")

const User = mongoose.Schema({
    userName:{
        type:String,
        required:true,
        maxLength:30,
        unique:true
    },
    accountNumber:{
        type:String,
        required:true
    },
    emailAddress:{
        type:String,
        required:true,
        unique:true
    },
    identityNumber:{
        type:Number,
        required:true,
        unique: true
    }
})

User.index({identityNumber:1, accountNumber:1});

module.exports = mongoose.model("user", User)