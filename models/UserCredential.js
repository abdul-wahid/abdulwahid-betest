const mongoose = require("mongoose")

const UserCredential = mongoose.Schema({
    emailAddress:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true,
        unique:true
    },
})

UserCredential.index({emailAddress:1})

module.exports = mongoose.model("user-credential", UserCredential)
