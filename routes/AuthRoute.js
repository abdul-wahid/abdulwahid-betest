const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require("../models/User")
const UserCredential = require("../models/UserCredential")
const {redis, clearCache} = require('../modules/redis');
const {saveTokenToRedis} = require("../controllers/AuthController")

router.post('/login', async (req, res) => {
    try {
        const { emailAddress, password } = req.body;

        // Cari user berdasarkan email
        const userCredential = await UserCredential.findOne({ emailAddress });

        if (!userCredential) {
            return res.status(401).json({ message: 'Login failed' });
        }

        // Bandingkan kata sandi yang dihash dengan kata sandi yang dimasukkan
        const passwordMatch = await bcrypt.compare(password, userCredential.password);

        if (!passwordMatch) {
            return res.status(401).json({ message: 'Login failed' });
        }

        // Buat token JWT
        let token = jwt.sign({ emailAddress: userCredential.emailAddress }, 'secret', { expiresIn: '1h' });
        saveTokenToRedis(emailAddress, token);

        res.status(200).json({ token });
    } catch (error) {
        res.status(500).json({ message: error });
    }
});

module.exports = router;