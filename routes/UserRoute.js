const express = require("express")
const router = express.Router()
const bcrypt = require('bcrypt');
const User = require("../models/User")
const UserCredential = require("../models/UserCredential")
const {redis, clearCache} = require('../modules/redis');
const {authenticateToken} = require("../controllers/AuthController")

// Middleware untuk caching
const cacheMiddleware = (req, res, next) => {
    const cacheKey = req.originalUrl; // Kunci cache berdasarkan ID pengguna
    redis.get(cacheKey, async (err, cachedData) => {
        if (err) {
            console.error('Redis Error: ', err);
            next(); // Jika ada kesalahan, lanjutkan ke handler berikutnya
        } else if (cachedData) {
            console.log("Cached Data is available")
            // Data ada di cache, kirimkan respons dari cache
            res.json(JSON.parse(cachedData));
        } else {
            console.log("Cached Data is not available")
            // Data tidak ada di cache, lanjutkan ke handler berikutnya
            next();
        }
    });
};

router.get("/", authenticateToken, cacheMiddleware, async (req, res) =>{
    try {
        const users = await User.find()

        // Simpan data ke cache dengan waktu kadaluarsa
        const cacheKey = req.originalUrl;
        redis.setex(cacheKey, 3600, JSON.stringify(users));

        res.json(users);
    }catch (e){
        res.json({message:e})
    }
})

router.get("/accountNumber/:accountNumber", authenticateToken, cacheMiddleware, async (req, res) => {
    try {
        const user = await User.findOne({ accountNumber: req.params.accountNumber });

        if (!user) {
            res.status(404).json({ message: "User not found" });
            return;
        }

        const cacheKey = req.originalUrl;
        redis.setex(cacheKey, 3600, JSON.stringify(user));
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});

router.get("/identityNumber/:identityNumber",authenticateToken, cacheMiddleware, async (req, res) => {
    try {
        const user = await User.findOne({ identityNumber: req.params.identityNumber });
        if (!user) {
            res.status(404).json({ message: "User not found" });
            return;
        }

        const cacheKey = req.originalUrl;
        redis.setex(cacheKey, 3600, JSON.stringify(user));
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});
router.post("/", async (req, res) => {
    try {
        const lastUser = await User.findOne({}, {}, { sort: { identityNumber: -1 } });
        const lastIdentityNumber = lastUser ? lastUser.identityNumber : 0;

        const newUser = new User({
            userName:req.body.userName,
            accountNumber:req.body.accountNumber,
            emailAddress:req.body.emailAddress,
            identityNumber: lastIdentityNumber + 1
        })

        let hashedPassword = await bcrypt.hash(req.body.password, 10);
        const newUserCredential = new UserCredential({
            emailAddress:req.body.emailAddress,
            password:hashedPassword
        })

        const user = await newUser.save()
        await newUserCredential.save()

        res.json(user)
        clearCache();
    }catch (e){
        res.json({message:e})
    }
})
router.put("/", authenticateToken, async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(
            req.query.userId,
            {
                userName: req.body.userName,
                emailAddress: req.body.emailAddress,
            },
            { new: true }
        );
        if (!updatedUser) {
            res.status(404).json({ message: "User not found" });
            return;
        }
        res.json(updatedUser);
        clearCache();
    } catch (e) {
        res.json({ message: e });
    }
});

router.delete("/:userId", authenticateToken, async (req, res) => {
    try {
        const removedUser = await User.findByIdAndRemove(req.params.userId);
        if (!removedUser) {
            res.status(404).json({ message: "User not found" });
            return;
        }
        res.json({ message: "User deleted successfully" });

        clearCache();
    } catch (e) {
        res.json({ message: e });
    }
});

module.exports = router