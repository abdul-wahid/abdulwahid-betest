const Redis = require("ioredis");
require("dotenv/config")
const redis = new Redis({
    password: process.env.REDIS_PWD,
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
});

console.log("9", process.env.PORT);

redis.on('connect', () => {
    console.log('Redis Cloud Connected!');
});

redis.on('error', (err) => {
    console.error('Redis connection error: ', err);
});

const clearCache = () => {
    redis.flushall((err, result) => {
        if (err) {
            console.error('Error deleting all cache: ', err);
        } else {
            console.log('All caches have been cleared: ', result);
        }
    });
}

module.exports = {redis, clearCache};