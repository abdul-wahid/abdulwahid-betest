const {redis, clearCache} = require('../modules/redis');
const jwt = require('jsonwebtoken');

const saveTokenToRedis = (emailAddress, token) => {
    redis.setex(`emailAddress:${emailAddress}`, 3600, token);
}

const authenticateToken = (req, res, next) => {
    let token = req.header('Authorization');
    token = token.replace('Bearer ', '');

    if (!token) {
        return res.status(401).json({ message: 'Access denied' });
    }

    jwt.verify(token, 'secret', (err, decodedToken) => {
        if (err) {
            return res.status(403).json({ message: err.message });
        }

        const emailAddress = decodedToken.emailAddress;

        // Cek apakah token ada dalam Redis
        redis.get(`emailAddress:${emailAddress}`, (err, cachedToken) => {
            if (err) {
                console.error('Redis Error: ', err);
                return res.status(500).json({ message: 'There is an error' });
            }

            console.log(token)
            console.log(cachedToken)
            if (token != cachedToken) {
                return res.status(401).json({ message: 'Access denied' });
            }

            req.emailAddress = emailAddress;
            next();
        });
    });
}

module.exports = {saveTokenToRedis, authenticateToken}