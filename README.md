**Authorization tipe** : Bearer Token
<br>
Menggunakan Redis Cloud

<br>
API - END POINT
<br>
<br>

Fungsi : Tambah User
<br>
Url : localhost:8080/user
<br>
Method : POST
<br>
Body
```json
{
    "userName":"abdulwahidk",
    "accountNumber":"321",
    "emailAddress":"emailHeruu@email.com",
    "password": "emailHeru"
}
```
<br>
Fungsi : Login
<br>
Url : localhost:8080/auth/login
<br>
Method : POST
<br>
Body

```json
{
    "emailAddress": "contohemail@email.com",
    "password":"password"
}
```
<br>
Fungsi : Menampilkan semua data user
<br>
Url : localhost:8080/user
<br>
Method : GET
<br>
<br>
Fungsi : Cari User berdasarkan accountNumber
<br>
Url : localhost:8080/user/accountNumber/12341
<br>
Method : GET
<br>
<br>
Fungsi : Cari User berdasarkan identityNumber
<br>
Url : localhost:8080/user/identityNumber/1231
<br>
Method : GET
<br>
<br>
Fungsi : Ubah data User berdasarkan _id
<br>
Url : localhost:8080/user?userId=6508288ab2651d9e59f526b6
<br>
Method : PUT
<br>
Body

```json
{
    "userName":"coba111Diubah",
    "emailAddress":"emaildiubah@email.com"
}
```
<br>
Fungsi : Menghapus data User
<br>
Url : localhost:8080/user/6508031536e28e5b254430ef
<br>
Method : DELETE
