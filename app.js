const express = require("express")
const app = express()
const mongoose = require("mongoose")
require("dotenv/config")
const userRoute = require("./routes/UserRoute")
const authRoute = require("./routes/AuthRoute")
const bodyParser = require("body-parser")

app.use(bodyParser())

// Listen
app.listen(process.env.PORT, () => {
    console.log(`Server running port: ${process.env.PORT}`)
})

//Connect to DB
mongoose.connect(process.env.DB_CONNECTION)
let db = mongoose.connection

db.on("error", console.error.bind(console, "Database connect Error!"))
db.once("open", () => {
    console.log("Database is connected")
})

app.use("/user", userRoute)
app.use("/auth", authRoute)